FROM node

COPY . /home/app

WORKDIR /home/app

ENTRYPOINT ["npm", "start"]